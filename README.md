# Recipe App API Proxy

NGINX proxy app for our recipe app

## Usage

## Environment Variables

* 'LISTEN_PORT' - Port to lsiten on (default: '8000')
* 'APP_HOST' - Hostname of the app to farward requests to (defaults: 'app')
* 'APP_PORT' - Port of the app to farward request to (default: '9000')